provider "aws" {
    region      = "eu-north-1"
    access_key  = "AKIAYILSSJOQ4LLWAXFN"
    secret_key  = "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"
}

variable "subnet_cidr_block" {
    description = "subnet cidr block"
}


resource "aws_vpc" "development-vpc" {
    cidr_block  = "10.0.0.0/16"
    tags        = {
        Name    : "development"
        vpc_env : "dev"
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id              = aws_vpc.development-vpc.id
    cidr_block          = var.subnet_cidr_block
    availability_zone   = "eu-north-1a"
    tags                = {
        Name: "subnet-1-dev"
    }
}

output "dev-vpc-id" {
  value       = aws_vpc.development-vpc.id
}
output "dev-subnet-id" {
  value       = aws_subnet.dev-subnet-1.id
}